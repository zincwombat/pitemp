-module(pitemp_gapi_server).
-behaviour(gen_server).

-export([start/0,
  stop/0,
  state/0,
  invoke/3]).

-include("debug.hrl").
-include("pienv.hrl").

-define(SERVERREF, {global, ?MODULE}).

-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-record(state, {auth}).

%==============================================================================
% API
%==============================================================================

start() ->
  gen_server:start_link(?SERVERREF, ?MODULE, [], []).

stop() ->
  gen_server:call(?SERVERREF, stop).

state() ->
  gen_server:call(?SERVERREF, state).

invoke(Module, Function, Args) ->
  gen_server:call(?SERVERREF, {invoke, Module, Function, Args}).


%==============================================================================
% callback functions
%==============================================================================

init([]) ->
  ?LOG_INFO({pid, self()}),
  process_flag(trap_exit, true),
  {ok, #state{}}.

handle_call(stop, _From, State) ->
  {stop, normal, ok, State};

handle_call(state, _From, State) ->
  {reply, {ok, State}, State};

handle_call({invoke, Module, Function, Args}, _From, State) ->
  Response = erlang:apply(Module, Function, Args),
  {reply, Response, State};

handle_call(Msg, From, State) ->
  Unhandled = {unhandled_call, {msg, Msg}, {from, From}},
  ?LOG_WARNING(Unhandled),
  {reply, Unhandled, State}.

handle_cast(Msg, State) ->
  Unhandled = {unhandled_cast, {msg, Msg}},
  ?LOG_WARNING(Unhandled),
  {noreply, State}.

handle_info(Msg, State) ->
  Unhandled = {unhandled_info, {msg, Msg}},
  ?LOG_WARNING(Unhandled),
  {noreply, State}.

code_change(_OldVsn, Ctx, _Extra) ->
  {ok, Ctx}.

terminate(Reason, #state{}) ->
  ?LOG_INFO({terminating, Reason}),
  ok.





