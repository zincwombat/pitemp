-module(pitemp_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).
-include("pienv.hrl").
-include("debug.hrl").

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
  ?LOG_INFO(starting),
  pitemp_sup:start_link().

stop(_State) ->
  ok.
