-module(pitemp_sensor).
-behaviour(gen_server).

-export([start/0,
  stop/0,
  state/0]).

-include("debug.hrl").
-include("pienv.hrl").

-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-record(state, {label = undefined,
  errorcount = 0,
  topic = undefined,
  readfail_flag = false,
  sysfile = undefined}).

%==============================================================================
% API
%==============================================================================

start() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

stop() ->
  gen_server:call(?MODULE, stop).

state() ->
  gen_server:call(?MODULE, state).

%==============================================================================
% callback functions
%==============================================================================

init(Args) ->
  ?LOG_INFO({started, {pid, self(), {args, Args}}}),
  process_flag(trap_exit, true),
  Topic = get_config(mqtt_publish_topic, ?DEFAULT_MQTT_PUBLISH_QUEUE),
  SysFile = get_config(sysfile, undefined),
  ClientName = get_config(mqtt_client_name, ?DEFAULT_MQTT_CLIENT_NAME),

  case SysFile of
    undefined ->
      ?LOG_ERROR({sensorfile, undefined}),
      {ok, #state{}};
    _ ->
      {ok, _} = timer:send_after(5000, check),
      {ok, _} = timer:send_interval(?TEMP_SENSOR_SCAN_INTERVAL, check),
      {ok, #state{sysfile = SysFile, label = ClientName, topic = Topic}}
  end.


handle_call(stop, _From, State) ->
  {stop, normal, ok, State};

handle_call(state, _From, State) ->
  {reply, {ok, State}, State};

handle_call(Msg, From, State) ->
  Unhandled = {unhandled_call, {msg, Msg}, {from, From}},
  ?LOG_WARNING(Unhandled),
  {reply, Unhandled, State}.

handle_cast(Msg, State) ->
  Unhandled = {unhandled_cast, {msg, Msg}},
  ?LOG_WARNING(Unhandled),
  {noreply, State}.

handle_info(check, State = #state{sysfile = SysFile, label = Label, errorcount = EC, topic = Topic}) ->
  % need to check for error condition in case sensor is not able
  % to be read
  Reply =
    case file:read_file(SysFile) of
      E = {error, _Reason} when EC < 5 ->
        NewEC = EC + 1,
        ?LOG_ERROR({sensor_read_file, {file, SysFile}, {errorcount, NewEC}, E}),
        {noreply, State#state{errorcount = NewEC}};

      E = {error, _Reason} ->
        ?LOG_ERROR({sensor_read_file, {file, SysFile}, E}),
        {stop, normal, State#state{errorcount = EC + 1, readfail_flag = true}};


      {ok, Bin} when is_binary(Bin) ->
        case parseResult(Bin) of
          {ok, TempStr} ->
            {Temp, _} = string:to_integer(TempStr),
            TimeStamp = os:system_time(),
            MQTTMessage = #{measurement => Label,
              fields => #{val => Temp},
              timestamp => TimeStamp},
            mqtt_publisher:publish(MQTTMessage, Topic);
          _ ->
            ok
        end,
        {noreply, State#state{errorcount = 0}}
    end,
  Reply;

handle_info(Msg, State) ->
  Unhandled = {unhandled_info, {msg, Msg}},
  ?LOG_WARNING(Unhandled),
  {noreply, State}.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

terminate(Reason, #state{readfail_flag = true, sysfile = SysFile}) ->
  ?LOG_WARNING({terminating, {read_fail, SysFile}, {reason, Reason}}),
  ok;

terminate(Reason, State) ->
  ?LOG_INFO({terminating, Reason, State}),
  ok.

% internal routines

get_config(Key, Default) ->
  {ok, HostName} = inet:gethostname(),
  HostKey = list_to_atom(HostName),

  Result =
    case application:get_env(?APPNAME, HostKey) of
      undefined ->
        Default;
      {ok, Config} when is_map(Config) ->
        maps:get(Key, Config, Default);
      Error ->
        ?LOG_ERROR({badconfig, Error}),
        Default
    end,
  ?LOG_INFO({lookup, Key, Result}),
  Result.


parseResult(Bin) when is_binary(Bin) ->
  case binary:split(Bin, <<"\n">>, [global, trim]) of
    [CRC, TEMP] ->
      case getCRC(CRC) of
        {ok, "YES"} ->
          getTemp(TEMP);

        Error ->
          Error
      end;
    _ ->
      {error, {parse_fail, Bin}}
  end.


getCRC(CRCBin) ->
  case re:run(CRCBin, "crc=.*(YES|NO)", [{capture, all_but_first, list}]) of
    {match, [Result]} ->
      {ok, Result};
    _ ->
      ?LOG_ERROR({parse_fail, CRCBin, {timestamp, unixEpochTime()}}),
      {error, {parse_fail, CRCBin}}
  end.

getTemp(TempBin) ->
  case re:run(TempBin, "t=(-?\\d+)", [{capture, all_but_first, list}]) of
    {match, [TempStr]} ->
      {ok, TempStr};
    _ ->
      ?LOG_ERROR({parse_fail, TempBin, {timestamp, unixEpochTime()}}),
      {error, {parse_fail, TempBin}}
  end.

unixEpochTime() ->
  os:system_time().
