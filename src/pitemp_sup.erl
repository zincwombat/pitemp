-module(pitemp_sup).

-behaviour(supervisor).

-include("debug.hrl").
-include("pienv.hrl").

%% API
-export([start_link/0,
  start/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor with args


-define(CHILD(I, Type, StartArgs), {I, {I, start, StartArgs}, permanent, 5000, Type, [I]}).


%% ===================================================================
%% API functions
%% ===================================================================

start() ->
  start_link().

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================
%% @private
init([]) ->
  ApiServer = ?CHILD(pitemp_gapi_server, worker, []),
  TempSensor = ?CHILD(pitemp_sensor, worker, []),
  SSpec = {ok, {{one_for_one, 5, 10}, [ApiServer, TempSensor]}},
  SSpec.


